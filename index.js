// dependency modules
const http = require('http')
const url = require('url').URL
const { StringDecoder } = require('string_decoder');

// create server
const server = http.createServer((req, res) => {
    
    // get the URL and parse it
    const inputUrl = req.url
    const baseUrl = 'http://localhost/'
    const url = new URL(inputUrl, baseUrl)
    
    // get the pathname
    const pathname = url.pathname.replace(/^\/+|\/+$/g, '') // replace all extra / like symbols at the back/front
    
    // get method
    const method = req.method.toLowerCase()
    
    // get query strings
    const queryStrings = url.searchParams

    // get headers
    const headers = url.headers

    // init string decoder
    const decoder = new StringDecoder() // default value is utf-8

    // init the buffer where all chunks data are stored
    let buffer = ''

    // read the data chunk from readable stream and store all the chunks till the end to buffer
    req.on('data', (chunk) => {
        buffer += decoder.write(chunk)
    })

    req.on('end', () => {
        buffer += decoder.end()
        
        console.log('user payload is: ' + buffer)
        // send the response
        res.end('Hello World')
    })    

})

// listen the server
server.listen(8081, () => {
    console.log('Server is listening on port 8081')
})